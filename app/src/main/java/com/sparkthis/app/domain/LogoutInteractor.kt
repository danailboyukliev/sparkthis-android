package com.sparkthis.app.domain

import com.sparkthis.app.network.BaseResponse
import com.sparkthis.app.network.RestApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

/**
 * Created by Admin on 10/24/2016.
 */
class LogoutInteractor(retrofit: Retrofit): UseCase<LogoutInteractor.Listener>(retrofit) {

    override fun execute() {
        val logoutCall = retrofit.create(RestApi::class.java).logout()

        logoutCall.enqueue(object: Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>?, response: Response<BaseResponse>?) {
                response?.let { response ->
                    response.body()?.let { body ->
                        if (body.success == true) {
                            listener?.onLogoutSuccess()
                        } else {
                            listener?.onLogoutFailure()
                        }
                        return
                    }
                }
                listener?.onFailure()
            }

            override fun onFailure(call: Call<BaseResponse>?, t: Throwable?) {
                listener?.onFailure()
            }

        })
    }


    interface Listener: UseCase.Listener {
        fun onLogoutSuccess()
        fun onLogoutFailure()
    }
}