package com.sparkthis.app.domain

import com.sparkthis.app.model.Post
import com.sparkthis.app.network.RestApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

/**
 * Created by Admin on 10/21/2016.
 */
class FetchPostsInteractor(retrofit: Retrofit): UseCase<FetchPostsInteractor.Listener>(retrofit) {
    override fun execute() {
        var call = retrofit.create(RestApi::class.java).fetchPosts()
        call.enqueue(object: Callback<List<Post>> {
            override fun onFailure(call: Call<List<Post>>?, t: Throwable?) {
                listener?.onFailure()
            }

            override fun onResponse(call: Call<List<Post>>?, response: Response<List<Post>>?) {
                response?.let { response ->
                    response.body()?.let { posts ->
                        listener?.onFetchPostsSuccess(posts)
                        return
                    }
                }
                listener?.onFailure()
            }
        })
    }

    interface Listener: UseCase.Listener {
        fun onFetchPostsFailure(message: String?)
        fun onFetchPostsSuccess(posts: List<Post>?)
    }
}