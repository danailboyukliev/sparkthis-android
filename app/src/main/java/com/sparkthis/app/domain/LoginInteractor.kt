package com.sparkthis.app.domain

import com.sparkthis.app.model.User
import com.sparkthis.app.network.LoginResponse
import com.sparkthis.app.network.RestApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

/**
 * Created by Admin on 10/21/2016.
 */
class LoginInteractor(retrofit: Retrofit): UseCase<LoginInteractor.Listener>(retrofit) {

    var email: String? = null
    var password: String? = null

    override fun execute() {
        var loginCall = email?.let { email ->
            password?.let { password ->
                retrofit
                    .create(RestApi::class.java)
                    .login(email, password)
            }
        }
        loginCall?.enqueue(object: Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                listener?.onFailure()
            }

            override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                response?.let { response ->
                    response.body()?.let { body ->
                        if (body.success == true) {
                            listener?.onLoginSuccess(body.message, body.user)
                        } else {
                            listener?.onLoginFailure(body.message)
                        }
                        return
                    }
                }
                listener?.onFailure()
            }
        })
    }

    interface Listener: UseCase.Listener {
        fun onLoginFailure(message: String?)
        fun onLoginSuccess(message: String?, user: User?)
    }
}