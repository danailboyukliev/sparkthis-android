package com.sparkthis.app.domain

import com.sparkthis.app.model.IdeaCategory
import com.sparkthis.app.model.TellMoreOption
import com.sparkthis.app.network.FetchIdeaOptionsResponse
import com.sparkthis.app.network.RestApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

/**
 * Created by Admin on 10/26/2016.
 */
class FetchIdeaOptionsInteractor(retrofit: Retrofit) : UseCase<FetchIdeaOptionsInteractor.Listener>(retrofit) {

    override fun execute() {
        val call = retrofit.create(RestApi::class.java).fetchIdeaOptions()
        call.enqueue(object: Callback<FetchIdeaOptionsResponse> {
            override fun onFailure(call: Call<FetchIdeaOptionsResponse>?, t: Throwable?) {
                listener?.onFailure()
            }

            override fun onResponse(call: Call<FetchIdeaOptionsResponse>?, response: Response<FetchIdeaOptionsResponse>?) {
                response?.let { response ->
                    response.body()?.let { body ->
                        listener?.onFetchIdeaOptionsSuccess(body.tellMoreOptions, body.categories)
                        return
                    }
                }
                listener?.onFetchIdeaOptionsFailure()
            }
        })
    }

    interface Listener: UseCase.Listener {
        fun onFetchIdeaOptionsSuccess(tellMoreOptions: List<TellMoreOption>, categories: List<IdeaCategory>)
        fun onFetchIdeaOptionsFailure()
    }
}