package com.sparkthis.app.domain

import com.sparkthis.app.model.IdeaCategory
import com.sparkthis.app.model.TellMoreOption
import com.sparkthis.app.model.User
import com.sparkthis.app.network.BaseResponse
import com.sparkthis.app.network.NormalResponse
import com.sparkthis.app.network.RestApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

/**
 * Created by Admin on 10/25/2016.
 */
class AddPostInteractor(user: User, retrofit: Retrofit) : UseCase<AddPostInteractor.Listener>(retrofit) {

    private var user: User

    var ideaTitle: String? = null
    var ideaShortDescription: String? = null
    var ideaBody: String? = null
    var ideaTellMoreOption: TellMoreOption? = null
    var ideaCategory: IdeaCategory? = null

    init {
        this.user = user
    }

    override fun execute() {
        var addPostCall = ideaTitle?.let { title ->
            ideaShortDescription?.let { shortDescription ->
                ideaBody?.let { ideaBody ->
                    ideaTellMoreOption?.let { tellMoreOption ->
                        ideaCategory?.let { category ->
                            retrofit.create(RestApi::class.java).postIdea(user.id.toString(),
                                    title,
                                    shortDescription,
                                    ideaBody,
                                    tellMoreOption.work.toString(),
                                    category.category.toString())
                        }
                    }
                }
            }
        }

        addPostCall?.enqueue(object: Callback<NormalResponse> {
            override fun onFailure(call: Call<NormalResponse>?, t: Throwable?) {
                t?.printStackTrace()
                listener?.onFailure()
            }

            override fun onResponse(call: Call<NormalResponse>?, response: Response<NormalResponse>?) {
                response?.let { response ->
                    response.body()?.let { body ->
                        if (body.success == true) {
                            listener?.onAddPostSuccess(body.message)
                        } else {
                            listener?.onAddPostFailure(body.message)
                        }
                        return
                    }
                }
                listener?.onFailure()
            }

        })
    }

    interface Listener: UseCase.Listener {
        fun onAddPostSuccess(message: String?)
        fun onAddPostFailure(message: String?)
    }
}