package com.sparkthis.app.domain

import com.sparkthis.app.network.RestApi
import retrofit2.Retrofit

/**
 * Created by Admin on 10/21/2016.
 */
abstract class UseCase<L: UseCase.Listener>(retrofit: Retrofit) {

    protected val retrofit: Retrofit
    var listener: L? = null

    init {
        this.retrofit = retrofit
    }

    abstract fun execute()

    interface Listener {
        fun onFailure()
    }
}