package com.sparkthis.app.domain

import com.sparkthis.app.network.BaseResponse
import com.sparkthis.app.network.LoginResponse
import com.sparkthis.app.network.RestApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

/**
 * Created by Admin on 10/25/2016.
 */
class RegisterInteractor(retrofit: Retrofit) : UseCase<RegisterInteractor.Listener>(retrofit) {

    var email: String? = null
    var password: String? = null

    override fun execute() {
        var registerCall = email?.let { email ->
            password?.let { password ->
                retrofit
                        .create(RestApi::class.java)
                        .register(email, password)
            }
        }
        registerCall?.enqueue(object: Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>?, t: Throwable?) {
                listener?.onFailure()
            }

            override fun onResponse(call: Call<BaseResponse>?, response: Response<BaseResponse>?) {
                response?.let { response ->
                    response.body()?.let { body ->
                        if (body.success == true) {
                            listener?.onRegisterSuccess(body.message)
                        } else {
                            listener?.onRegisterFailure(body.message)
                        }
                        return
                    }
                }
                listener?.onFailure()
            }
        })
    }

    interface Listener: UseCase.Listener {
        fun onRegisterSuccess(message: String?)
        fun onRegisterFailure(message: String?)
    }
}