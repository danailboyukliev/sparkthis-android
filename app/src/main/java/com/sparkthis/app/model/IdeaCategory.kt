package com.sparkthis.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Admin on 10/26/2016.
 */
class IdeaCategory {

    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("category")
    @Expose
    var category: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("order")
    @Expose
    var order: Int? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null

}
