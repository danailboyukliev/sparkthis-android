package com.sparkthis.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class User {

    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("user_social_id")
    @Expose
    var userSocialId: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("avatar")
    @Expose
    var avatar: String? = null

    @SerializedName("provider")
    @Expose
    var provider: String? = null

    @SerializedName("admin")
    @Expose
    var admin: String? = null

    @SerializedName("password_temp")
    @Expose
    var passwordTemp: String? = null

    @SerializedName("code")
    @Expose
    var code: String? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null

}
