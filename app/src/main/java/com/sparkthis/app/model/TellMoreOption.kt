package com.sparkthis.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Admin on 10/26/2016.
 */
class TellMoreOption {

    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("work")
    @Expose
    var work: String? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
}
