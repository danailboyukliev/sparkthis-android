package com.sparkthis.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Admin on 10/21/2016.
 */
class Post {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("user_id")
    @Expose
    var userId: Int? = null

    @SerializedName("category_id")
    @Expose
    var categoryId: Int? = null

    @SerializedName("tell_more_id")
    @Expose
    var tellMoreId: Int? = null

    @SerializedName("all_votes")
    @Expose
    var allVotes: Int? = null

    @SerializedName("all_comments")
    @Expose
    var allComments: String? = null

    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("short_desc")
    @Expose
    var shortDesc: String? = null

    @SerializedName("body")
    @Expose
    var body: String? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
}
