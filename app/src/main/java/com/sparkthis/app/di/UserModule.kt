package com.sparkthis.app.di

import com.sparkthis.app.model.User
import dagger.Module
import dagger.Provides

/**
 * Created by Admin on 10/21/2016.
 */
@Module
class UserModule(user: User) {

    private var user: User

    init {
        this.user = user
    }

    @UserScope
    @Provides
    fun provideUser() = user
}