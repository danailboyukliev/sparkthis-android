package com.sparkthis.app.di

import com.sparkthis.app.SparkThisApp
import com.sparkthis.app.ui.login.LoginActivityComponent
import com.sparkthis.app.ui.login.LoginActivityModule
import com.sparkthis.app.network.NetworkModule
import com.sparkthis.app.ui.register.RegisterActivityComponent
import com.sparkthis.app.ui.register.RegisterActivityModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Admin on 10/20/2016.
 */
@Singleton
@Component(
        modules = arrayOf(
                AndroidModule::class,
                NetworkModule::class
        )
)
interface AppComponent {
    fun inject(app: SparkThisApp)
    fun plus(loginActivityModule: LoginActivityModule): LoginActivityComponent
    fun plus(registerActivityModule: RegisterActivityModule): RegisterActivityComponent
    fun plus(userModule: UserModule): UserComponent
}