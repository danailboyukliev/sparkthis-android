package com.sparkthis.app.di

import android.app.Application
import com.sparkthis.app.SparkThisApp
import dagger.Module
import dagger.Provides
import timber.log.Timber
import javax.inject.Singleton

/**
 * Created by Admin on 10/20/2016.
 */
@Module
class AndroidModule(app: SparkThisApp) {

    private var app: SparkThisApp

    init {
        this.app = app
    }

    @Singleton
    @Provides
    fun provideApplication(): Application = app

    @Singleton
    @Provides
    fun provideTimberDebugTree(): Timber.DebugTree = Timber.DebugTree()
}
