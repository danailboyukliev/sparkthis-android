package com.sparkthis.app.di

import com.sparkthis.app.ui.addpost.AddPostActivityComponent
import com.sparkthis.app.ui.addpost.AddPostActivityModule
import com.sparkthis.app.ui.main.MainActivityComponent
import com.sparkthis.app.ui.main.MainActivityModule
import com.sparkthis.app.ui.post.PostActivityComponent
import com.sparkthis.app.ui.post.PostActivityModule
import dagger.Subcomponent

/**
 * Created by Admin on 10/21/2016.
 */
@UserScope
@Subcomponent(
        modules = arrayOf(UserModule::class)
)
interface UserComponent {
    fun plus(mainActivityModule: MainActivityModule): MainActivityComponent
    fun plus(addPostActivityModule: AddPostActivityModule): AddPostActivityComponent
    fun plus(postActivityModule: PostActivityModule): PostActivityComponent
}