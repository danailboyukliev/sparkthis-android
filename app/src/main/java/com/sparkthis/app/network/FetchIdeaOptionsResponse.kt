package com.sparkthis.app.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.sparkthis.app.model.IdeaCategory
import com.sparkthis.app.model.TellMoreOption

import java.util.ArrayList

/**
 * Created by Admin on 10/26/2016.
 */
class FetchIdeaOptionsResponse {

    @SerializedName("tell-more-options")
    @Expose
    var tellMoreOptions: List<TellMoreOption> = ArrayList()

    @SerializedName("categories")
    @Expose
    var categories: List<IdeaCategory> = ArrayList()
}
