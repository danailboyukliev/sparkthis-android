package com.sparkthis.app.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.sparkthis.app.model.User

/**
 * Created by Admin on 10/21/2016.
 */
class LoginResponse: BaseResponse() {

    @SerializedName("user")
    @Expose
    var user: User? = null
}