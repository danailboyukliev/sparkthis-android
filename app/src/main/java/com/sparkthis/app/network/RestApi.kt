package com.sparkthis.app.network

import com.sparkthis.app.model.IdeaCategory
import com.sparkthis.app.model.Post
import com.sparkthis.app.model.TellMoreOption
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by Admin on 10/20/2016.
 */
interface RestApi {
    @POST("sign-in")
    @FormUrlEncoded
    fun login(@Field("email") email: String,
              @Field("password") password: String): Call<LoginResponse>

    @POST("register")
    @FormUrlEncoded
    fun register(@Field("email") email: String,
                 @Field("password") password: String): Call<BaseResponse>

    @GET("logout")
    fun logout(): Call<BaseResponse>

    @GET("posts")
    fun fetchPosts(): Call<List<Post>>

    @POST("post-idea")
    @FormUrlEncoded
    fun postIdea(@Field("user_id") userId: String,
                 @Field("title") title: String,
                 @Field("short_desc") shortDescription: String,
                 @Field("body") body: String,
                 @Field("tell_more") tellMoreOption: String,
                 @Field("category") category: String): Call<NormalResponse>

    @GET("get-idea-options")
    fun fetchIdeaOptions(): Call<FetchIdeaOptionsResponse>
}