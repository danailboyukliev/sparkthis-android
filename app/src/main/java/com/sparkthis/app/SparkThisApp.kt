package com.sparkthis.app

import android.app.Application
import com.sparkthis.app.di.*
import com.sparkthis.app.model.User
import com.sparkthis.app.network.NetworkModule
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Admin on 10/20/2016.
 */
class SparkThisApp: Application() {

    companion object {
        lateinit var component: AppComponent
    }

    @Inject
    lateinit var debugTree: Timber.DebugTree

    private var userComponent: UserComponent? = null

    override fun onCreate() {
        super.onCreate()

        buildAppComponent()
        injectAppComponent()

        setupTimber()
    }

    fun buildAppComponent() {
        component = DaggerAppComponent.builder()
                .androidModule(AndroidModule(this))
                .networkModule(NetworkModule("http://192.168.0.111/sparkthis/public/rest/"))
                .build()
    }

    fun injectAppComponent() {
        component.inject(this)
    }

    fun getComponent(): AppComponent {
        return component
    }

    fun setupTimber() {
        if (BuildConfig.DEBUG) Timber.plant(debugTree)
    }

    fun buildUserComponent(user: User) {
        userComponent = component.plus(UserModule(user))
    }

    fun getUserComponent(): UserComponent? {
        return userComponent
    }

    fun releaseUserComponent() {
        userComponent = null
    }
}