package com.sparkthis.app.ui.addpost

import com.sparkthis.app.di.ActivityScope
import com.sparkthis.app.domain.AddPostInteractor
import com.sparkthis.app.domain.FetchIdeaOptionsInteractor
import com.sparkthis.app.model.User
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by Admin on 10/25/2016.
 */
@Module
class AddPostActivityModule(activity: AddPostActivity) {

    private var activity: AddPostActivity

    init {
        this.activity = activity
    }

    @ActivityScope
    @Provides
    fun provideFetchTellMoreOptionsInteractor(retrofit: Retrofit) = FetchIdeaOptionsInteractor(retrofit)

    @ActivityScope
    @Provides
    fun provideAddPostInteractor(user: User, retrofit: Retrofit) = AddPostInteractor(user, retrofit)

    @ActivityScope
    @Provides
    fun provideTellMoreRecyclerViewAdapter(): TellMoreRecyclerViewAdapter = TellMoreRecyclerViewAdapter()

    @ActivityScope
    @Provides
    fun provideIdeaCategoriesRecyclerViewAdapter(): IdeaCategoryRecyclerViewAdapter = IdeaCategoryRecyclerViewAdapter()

    @ActivityScope
    @Provides
    fun provideAddPostActivityPresenter(fetchTellMoreOptionsInteractor: FetchIdeaOptionsInteractor,
                                        addPostInteractor: AddPostInteractor)
            = AddPostActivityPresenter(fetchTellMoreOptionsInteractor, addPostInteractor)
}