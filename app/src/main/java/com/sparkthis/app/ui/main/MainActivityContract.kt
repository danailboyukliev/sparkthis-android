package com.sparkthis.app.ui.main

import com.sparkthis.app.ui.base.BaseNavigator
import com.sparkthis.app.ui.base.BasePresenter
import com.sparkthis.app.ui.base.BaseView
import com.sparkthis.app.model.Post

/**
 * Created by Admin on 10/21/2016.
 */
interface MainActivityContract {
    interface View: BaseView {
        fun releaseUserComponent()
        fun displayPosts(posts: List<Post>)
    }

    interface Navigator: BaseNavigator {
        fun navigateToLogin()
        fun navigateToAddPost()
        fun navigateToPost(post: Post)
    }

    abstract class Presenter: BasePresenter<View, Navigator>() {
        abstract fun logout()
        abstract fun fetchPosts()
    }
}