package com.sparkthis.app.ui.base

/**
 * Created by Admin on 10/20/2016.
 */
abstract class BasePresenter<T: BaseView, N: BaseNavigator> {

    var view: T? = null
    var navigator: N? = null

    fun bindView(view: T) {
        this.view = view
    }

    fun unbindView() {
        this.view = null
    }

    fun bindNavigator(navigator: N) {
        this.navigator = navigator
    }

    fun unbindNavigator() {
        this.navigator = null
    }
}