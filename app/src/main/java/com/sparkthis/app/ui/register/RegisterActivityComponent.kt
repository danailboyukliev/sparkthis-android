package com.sparkthis.app.ui.register

import com.sparkthis.app.di.ActivityScope
import dagger.Subcomponent

/**
 * Created by Admin on 10/25/2016.
 */
@ActivityScope
@Subcomponent(
        modules = arrayOf(RegisterActivityModule::class)
)
interface RegisterActivityComponent {
    fun inject(activity: RegisterActivity)
}