package com.sparkthis.app.ui.register

import android.app.ProgressDialog
import com.sparkthis.app.R
import com.sparkthis.app.di.ActivityScope
import com.sparkthis.app.domain.LoginInteractor
import com.sparkthis.app.domain.RegisterInteractor
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by Admin on 10/25/2016.
 */
@Module
class RegisterActivityModule(activity: RegisterActivity) {

    var activity: RegisterActivity

    init {
        this.activity = activity
    }

    @ActivityScope
    @Provides
    fun provideProgressDialog(): ProgressDialog {
        val progressDialog = ProgressDialog(activity, R.style.AppTheme_Dark_Dialog)
        progressDialog.isIndeterminate = true
        return progressDialog
    }

    @ActivityScope
    @Provides
    fun provideRegisterInteractor(retrofit: Retrofit) = RegisterInteractor(retrofit)

    @ActivityScope
    @Provides
    fun provideLoginInteractor(retrofit: Retrofit) = LoginInteractor(retrofit)

    @ActivityScope
    @Provides
    fun provideRegisterActivityPresenter(registerInteractor: RegisterInteractor, loginInteractor: LoginInteractor)
            = RegisterActivityPresenter(registerInteractor, loginInteractor)
}