package com.sparkthis.app.ui.login

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick

import com.sparkthis.app.R
import com.sparkthis.app.SparkThisApp
import com.sparkthis.app.ui.main.MainActivity
import com.sparkthis.app.model.User
import com.sparkthis.app.ui.register.RegisterActivity
import retrofit2.Retrofit
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), LoginActivityContract.View, LoginActivityContract.Navigator {

    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar

    @BindView(R.id.fab)
    lateinit var fab: FloatingActionButton

    @BindView(R.id.input_email)
    lateinit var inputEmailEditText: EditText

    @BindView(R.id.input_password)
    lateinit var inputPasswordEditText: EditText

    @OnClick(R.id.fab)
    fun onFabClicked(view: View) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show()
    }

    @OnClick(R.id.btn_login)
    fun onLoginButtonClicked(view: View) {
        // TODO: validation

        // if validation is passed
        showProgressDialog("Authenticating", 5000)

//        presenter.attemptLogin(inputEmailEditText.text.toString(), inputPasswordEditText.text.toString())
        presenter.attemptLogin(account_email, account_password)
    }

    @OnClick(R.id.link_signup)
    fun onSignupButtonClicked() {
        navigateToRegister()
    }

    @Inject
    lateinit var presenter: LoginActivityPresenter

    @Inject
    lateinit var retrofit: Retrofit

    @Inject
    lateinit var loginProgressDialog: ProgressDialog

    val account_email: String = "mihailkolimechkov@gmail.com"
    val account_password: String = "00120640788719101"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        ButterKnife.bind(this)

        setSupportActionBar(toolbar)

        (application as SparkThisApp).getComponent().plus(LoginActivityModule(this)).inject(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.bindView(this)
        presenter.bindNavigator(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.unbindView()
        presenter.unbindNavigator()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unbindView()
        presenter.unbindNavigator()
    }

    override fun showProgressDialog(message: String, length: Long) {
        loginProgressDialog.hide()
        loginProgressDialog.setMessage(message)
        loginProgressDialog.show()
    }

    override fun hideProgressDialog() {
        loginProgressDialog.hide()
    }

    override fun buildUserComponent(user: User) {
        (application as SparkThisApp).buildUserComponent(user)
    }

    override fun navigateToMain(delay: Long) {
        Handler().postDelayed({
            runOnUiThread {
                startActivity(Intent(LoginActivity@this, MainActivity::class.java))
                finish()
            }
        }, delay)
    }

    override fun navigateToRegister() {
        startActivity(Intent(LoginActivity@this, RegisterActivity::class.java))
        finish()
    }
}
