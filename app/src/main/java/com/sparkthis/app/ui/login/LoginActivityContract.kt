package com.sparkthis.app.ui.login

import com.sparkthis.app.ui.base.BaseNavigator
import com.sparkthis.app.ui.base.BasePresenter
import com.sparkthis.app.ui.base.BaseView
import com.sparkthis.app.model.User

/**
 * Created by Admin on 10/20/2016.
 */
interface LoginActivityContract {
    interface View: BaseView {
        fun buildUserComponent(user: User)
        fun showProgressDialog(message: String, length: Long)
        fun hideProgressDialog()
    }

    interface Navigator: BaseNavigator {
        fun navigateToMain(delay: Long)
        fun navigateToRegister()
    }

    abstract class Presenter: BasePresenter<View, Navigator>() {
        abstract fun attemptLogin(email: String, password: String)
    }
}