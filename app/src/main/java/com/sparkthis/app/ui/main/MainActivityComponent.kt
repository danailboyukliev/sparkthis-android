package com.sparkthis.app.ui.main

import com.sparkthis.app.di.ActivityScope
import dagger.Subcomponent

/**
 * Created by Admin on 10/21/2016.
 */
@ActivityScope
@Subcomponent(modules = arrayOf(MainActivityModule::class))
interface MainActivityComponent {
    fun inject(activity: MainActivity)
}