package com.sparkthis.app.ui.addpost

import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.widget.AutoCompleteTextView
import android.widget.Button
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick

import com.sparkthis.app.R
import com.sparkthis.app.SparkThisApp
import com.sparkthis.app.model.IdeaCategory
import com.sparkthis.app.model.TellMoreOption
import javax.inject.Inject

class AddPostActivity : AppCompatActivity(), AddPostActivityContract.View, AddPostActivityContract.Navigator {

    @BindView(R.id.toolbar) lateinit var toolbar: Toolbar
    @BindView(R.id.title_edit_text) lateinit var titleEditText: AutoCompleteTextView
    @BindView(R.id.short_description_edit_text) lateinit var shortDescriptionEditText: AutoCompleteTextView
    @BindView(R.id.details_edit_text) lateinit var detailsEditText: AutoCompleteTextView
    @BindView(R.id.tell_more_recycler_view) lateinit var tellMoreRecyclerView: RecyclerView
    @BindView(R.id.categories_recycler_view) lateinit var categoriesRecyclerView: RecyclerView
    @BindView(R.id.add_post_button) lateinit var addPostButton: Button

    @OnClick(R.id.add_post_button)
    fun onAddPostButtonClicked() {
        if (tellMoreOptionsAdapter.checked == null) {
            displayError("Select at least one checkbox from Tell Us More section.")
            return
        }

        if (ideaCategoriesAdapter.checked == null) {
            displayError("Select at least one checkbox from Categories section.")
            return
        }

        presenter.addIdea(title = titleEditText.text.toString(),
                shortDescription = shortDescriptionEditText.text.toString(),
                body = detailsEditText.text.toString(),
                tellMoreOption = tellMoreOptionsAdapter.checked!!,
                category = ideaCategoriesAdapter.checked!!)
    }

    @Inject lateinit var presenter: AddPostActivityPresenter
    @Inject lateinit var tellMoreOptionsAdapter: TellMoreRecyclerViewAdapter
    @Inject lateinit var ideaCategoriesAdapter: IdeaCategoryRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)

        ButterKnife.bind(this)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        (application as SparkThisApp).getUserComponent()?.plus(AddPostActivityModule(this))?.inject(this)

        tellMoreRecyclerView.setHasFixedSize(false)
        tellMoreRecyclerView.layoutManager = GridLayoutManager(this, 2)
        tellMoreRecyclerView.adapter = tellMoreOptionsAdapter

        categoriesRecyclerView.setHasFixedSize(false)
        categoriesRecyclerView.layoutManager = GridLayoutManager(this, 2)
        categoriesRecyclerView.adapter = ideaCategoriesAdapter
    }

    override fun onResume() {
        super.onResume()
        presenter.bindView(this)
        presenter.bindNavigator(this)
        presenter.fetchIdeaOptions()
    }

    override fun onStop() {
        super.onStop()
        presenter.unbindView()
        presenter.unbindNavigator()
    }

    override fun displayError(text: String) {
        Snackbar.make(findViewById(R.id.content), text, Snackbar.LENGTH_LONG).show()
    }

    override fun displayIdeaOptions(tellMoreOptions: List<TellMoreOption>, categories: List<IdeaCategory>) {
        tellMoreOptionsAdapter.update(tellMoreOptions)
        ideaCategoriesAdapter.update(categories)
    }

    override fun navigateBack(delay: Long) {
        Handler().postDelayed({
            runOnUiThread {
                finish() }
        }, delay)
    }
}
