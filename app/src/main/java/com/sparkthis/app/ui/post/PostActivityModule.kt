package com.sparkthis.app.ui.post

import com.sparkthis.app.di.ActivityScope
import dagger.Module
import dagger.Provides

/**
 * Created by Admin on 11/4/2016.
 */
@Module
class PostActivityModule(postActivity: PostActivity) {

    private val postActivity: PostActivity

    init {
        this.postActivity = postActivity
    }

    @ActivityScope
    @Provides
    fun providePostActivityPresenter() = PostActivityPresenter()
}