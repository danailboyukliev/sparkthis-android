package com.sparkthis.app.ui.login

import com.sparkthis.app.di.ActivityScope
import dagger.Subcomponent

/**
 * Created by Admin on 10/20/2016.
 */
@ActivityScope
@Subcomponent(
        modules = arrayOf(
                LoginActivityModule::class
        )
)
interface LoginActivityComponent {
    fun inject(activity: LoginActivity)
}