package com.sparkthis.app.ui.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.sparkthis.app.R
import com.sparkthis.app.model.Post

/**
 * Created by Admin on 10/21/2016.
 */
class PostsRecyclerViewAdapter: RecyclerView.Adapter<PostsRecyclerViewAdapter.ViewHolder>() {

    private var posts: List<Post>
    var navigator: MainActivityContract.Navigator? = null

    init {
        this.posts = mutableListOf<Post>()
    }

    fun updatePosts(posts: List<Post>) {
        this.posts = posts
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_post, parent, false)
        val viewHolder = ViewHolder(view)
        return viewHolder
    }

    override fun getItemCount(): Int = posts.count()

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.let { holder ->
            holder.voteCounterTextView!!.text = posts[position].allVotes.toString()
            holder.titleTextView!!.text = posts[position].title
            holder.shortDescriptionTextView!!.text = posts[position].shortDesc
            holder.view?.setOnClickListener { view ->
                navigator?.navigateToPost(post = posts[position])
            }
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var voteCounterTextView: TextView? = null
        var titleTextView: TextView? = null
        var shortDescriptionTextView: TextView? = null
        var view: View? = null

        init {
            this.view = itemView
            itemView?.let { itemView ->
                voteCounterTextView = itemView.findViewById(R.id.vote_counter_text_view) as TextView
                titleTextView = itemView.findViewById(R.id.title_text_view) as TextView
                shortDescriptionTextView = itemView.findViewById(R.id.short_description_text_view) as TextView
            }
        }
    }
}