package com.sparkthis.app.ui.addpost

import com.sparkthis.app.di.ActivityScope
import dagger.Subcomponent

/**
 * Created by Admin on 10/25/2016.
 */
@ActivityScope
@Subcomponent(
        modules = arrayOf(AddPostActivityModule::class)
)
interface AddPostActivityComponent {
    fun inject(activity: AddPostActivity)
}