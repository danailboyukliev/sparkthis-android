package com.sparkthis.app.ui.post

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick

import com.sparkthis.app.R
import com.sparkthis.app.SparkThisApp
import com.sparkthis.app.model.Post
import com.sparkthis.app.ui.addpost.AddPostActivityModule
import javax.inject.Inject

class PostActivity : AppCompatActivity(), PostActivityContract.View, PostActivityContract.Navigator {

    @BindView(R.id.toolbar) lateinit var toolbar: Toolbar
    @BindView(R.id.fab) lateinit var fab: FloatingActionButton

    @OnClick(R.id.fab)
    fun onFabClick() {

    }

    @Inject lateinit var presenter: PostActivityPresenter

    private var post: Post = Post()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        ButterKnife.bind(this)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        buildPostFromIntent(intent)

        (application as SparkThisApp).getUserComponent()?.plus(PostActivityModule(this))?.inject(this)
    }

    private fun buildPostFromIntent(intent: Intent) {
        post.id = intent.extras.getInt("id")
        post.userId = intent.extras.getInt("user_id")
        post.categoryId = intent.extras.getInt("category_id")
        post.tellMoreId = intent.extras.getInt("tell_more_id")
        post.allVotes = intent.extras.getInt("all_votes")
        post.allComments = intent.extras.getString("all_comments")
        post.title = intent.extras.getString("title")
        post.shortDesc = intent.extras.getString("short_desc")
        post.body = intent.extras.getString("body")
        post.createdAt = intent.extras.getString("created_at")
        post.updatedAt = intent.extras.getString("updated_at")
    }
}
