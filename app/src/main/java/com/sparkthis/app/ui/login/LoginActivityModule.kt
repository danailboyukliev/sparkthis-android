package com.sparkthis.app.ui.login

import android.app.ProgressDialog
import com.sparkthis.app.R
import com.sparkthis.app.di.ActivityScope
import com.sparkthis.app.domain.LoginInteractor
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by Admin on 10/20/2016.
 */
@Module
class LoginActivityModule(activity: LoginActivity) {

    private var activity: LoginActivity

    init {
        this.activity = activity
    }

    @ActivityScope
    @Provides
    fun provideProgressDialog(): ProgressDialog {
        val progressDialog = ProgressDialog(activity, R.style.AppTheme_Dark_Dialog)
        progressDialog.isIndeterminate = true
        return progressDialog
    }

    @ActivityScope
    @Provides
    fun provideLoginInteractor(retrofit: Retrofit): LoginInteractor {
        return LoginInteractor(retrofit)
    }

    @ActivityScope
    @Provides
    fun providePresenter(loginInteractor: LoginInteractor) = LoginActivityPresenter(loginInteractor)
}
