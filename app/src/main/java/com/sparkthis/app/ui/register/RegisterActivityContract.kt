package com.sparkthis.app.ui.register

import com.sparkthis.app.model.User
import com.sparkthis.app.ui.base.BaseNavigator
import com.sparkthis.app.ui.base.BasePresenter
import com.sparkthis.app.ui.base.BaseView

/**
 * Created by Admin on 10/25/2016.
 */
interface RegisterActivityContract {

    interface View: BaseView {
        fun buildUserComponent(user: User)
        fun showProgressDialog(message: String, length: Long)
        fun hideProgressDialog()
    }

    interface Navigator: BaseNavigator {
        fun navigateToMain(delay: Long)
        fun navigateToLogin()
    }

    abstract class Presenter: BasePresenter<View, Navigator>() {

    }

}