package com.sparkthis.app.ui.addpost

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.sparkthis.app.R
import com.sparkthis.app.model.TellMoreOption

/**
 * Created by Admin on 10/26/2016.
 */
class TellMoreRecyclerViewAdapter(): RecyclerView.Adapter<TellMoreRecyclerViewAdapter.ViewHolder>() {

    var checked: TellMoreOption? = null

    private val elements: MutableList<TellMoreOption>
    private var checkboxes: MutableList<CheckBox>

    init {
        this.elements = mutableListOf()
        this.checkboxes = mutableListOf()
    }

    fun update(elements: List<TellMoreOption>) {
        this.elements.addAll(elements)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: TellMoreRecyclerViewAdapter.ViewHolder?, position: Int) {
        holder?.let { holder ->
            holder.labelTextView?.text = elements[position].work
            holder.checkbox?.let { checkboxes.add(it) }
            holder.checkbox?.setOnCheckedChangeListener { compoundButton, isChecked: Boolean ->

                // uncheck all other checkboxes
                checkboxes.forEach { it.isChecked = false }

                holder.checkbox?.isChecked = isChecked
                if (isChecked) {
                    checked = elements[position]
                } else {
                    checked = null
                }
            }
        }
    }

    override fun getItemCount(): Int = elements.count()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TellMoreRecyclerViewAdapter.ViewHolder {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_checkbox_grid, parent, false)
        return TellMoreRecyclerViewAdapter.ViewHolder(view)
    }


    class ViewHolder(itemView: View?): RecyclerView.ViewHolder(itemView) {

        var checkbox: CheckBox? = null
        var labelTextView: TextView? = null

        init {
            itemView?.let { itemView ->
                checkbox = itemView.findViewById(R.id.checkbox_value) as CheckBox
                labelTextView = itemView.findViewById(R.id.checkbox_label) as TextView
            }
        }
    }
}