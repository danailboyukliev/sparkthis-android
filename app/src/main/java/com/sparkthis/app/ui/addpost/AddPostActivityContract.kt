package com.sparkthis.app.ui.addpost

import com.sparkthis.app.model.IdeaCategory
import com.sparkthis.app.model.TellMoreOption
import com.sparkthis.app.ui.base.BaseNavigator
import com.sparkthis.app.ui.base.BasePresenter
import com.sparkthis.app.ui.base.BaseView

/**
 * Created by Admin on 10/25/2016.
 */
interface AddPostActivityContract {

    interface View: BaseView {
        fun displayIdeaOptions(tellMoreOptions: List<TellMoreOption>, categories: List<IdeaCategory>)
        fun displayError(text: String)
    }

    interface Navigator: BaseNavigator {
        fun navigateBack(delay: Long)
    }

    abstract class Presenter: BasePresenter<View, Navigator>() {
        abstract fun fetchIdeaOptions()
        abstract fun addIdea(title: String, shortDescription: String, body: String, tellMoreOption: TellMoreOption, category: IdeaCategory)
    }

}