package com.sparkthis.app.ui.post

import com.sparkthis.app.di.ActivityScope
import dagger.Subcomponent

/**
 * Created by Admin on 11/4/2016.
 */
@ActivityScope
@Subcomponent(
        modules = arrayOf(PostActivityModule::class)
)
interface PostActivityComponent {
    fun inject(postActivity: PostActivity)
}