package com.sparkthis.app.ui.register

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.EditText
import butterknife.BindString
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick

import com.sparkthis.app.R
import com.sparkthis.app.SparkThisApp
import com.sparkthis.app.model.User
import com.sparkthis.app.ui.login.LoginActivity
import com.sparkthis.app.ui.main.MainActivity
import javax.inject.Inject

class RegisterActivity: AppCompatActivity(), RegisterActivityContract.View, RegisterActivityContract.Navigator {

    @BindString(R.string.title_activity_login) lateinit var title: String
    @BindView(R.id.toolbar) lateinit var toolbar: Toolbar
    @BindView(R.id.input_email) lateinit var inputEmailEditText: EditText
    @BindView(R.id.input_password) lateinit var inputPasswordEditText: EditText
    @BindView(R.id.input_confirmed_password) lateinit var inputConfirmPasswordEditText: EditText

    @OnClick(R.id.button_register)
    fun onRegisterButtonClick(view: View) {
        // validate
        if (inputPasswordEditText.text.toString() != inputConfirmPasswordEditText.text.toString()) {
            return
        }

        presenter.register(inputEmailEditText.text.toString(), inputPasswordEditText.text.toString())
    }

    @OnClick(R.id.link_signin)
    fun onLinkToSigninClick(view: View) {
        navigateToLogin()
    }

    @Inject lateinit var progressDialog: ProgressDialog
    @Inject lateinit var presenter: RegisterActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        ButterKnife.bind(this)

        setSupportActionBar(toolbar)
        setTitle(title)

        (application as SparkThisApp).getComponent().plus(RegisterActivityModule(this)).inject(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.bindView(this)
        presenter.bindView(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.unbindView()
        presenter.unbindView()
    }

    override fun buildUserComponent(user: User) {
        (application as SparkThisApp).buildUserComponent(user)
    }

    override fun showProgressDialog(message: String, length: Long) {
        progressDialog.hide()
        progressDialog.setMessage(message)
        progressDialog.show()
    }

    override fun hideProgressDialog() {
        progressDialog.hide()
    }

    override fun navigateToLogin() {
        startActivity(Intent(RegisterActivity@this, LoginActivity::class.java))
        finish()
    }

    override fun navigateToMain(delay: Long) {
        Handler().postDelayed({
            runOnUiThread {
                startActivity(Intent(RegisterActivity@this, MainActivity::class.java))
                finish()
            }
        }, delay)
    }
}
