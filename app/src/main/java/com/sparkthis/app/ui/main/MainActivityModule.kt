package com.sparkthis.app.ui.main

import com.sparkthis.app.di.ActivityScope
import com.sparkthis.app.domain.FetchPostsInteractor
import com.sparkthis.app.domain.LogoutInteractor
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by Admin on 10/21/2016.
 */
@Module
class MainActivityModule(activity: MainActivity) {

    var activity: MainActivity

    init {
        this.activity = activity
    }

    @ActivityScope
    @Provides
    fun providePostsRecyclerViewAdapter() = PostsRecyclerViewAdapter()

    @ActivityScope
    @Provides
    fun provideLogoutInteractor(retrofit: Retrofit) = LogoutInteractor(retrofit)

    @ActivityScope
    @Provides
    fun provideFetchPostsInteractor(retrofit: Retrofit) = FetchPostsInteractor(retrofit)

    @ActivityScope
    @Provides
    fun provideMainActivityPresenter(logoutInteractor: LogoutInteractor, fetchPostsInteractor: FetchPostsInteractor)
            = MainActivityPresenter(logoutInteractor, fetchPostsInteractor)
}