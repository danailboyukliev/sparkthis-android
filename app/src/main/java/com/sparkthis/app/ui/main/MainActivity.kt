package com.sparkthis.app.ui.main

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick

import com.sparkthis.app.R
import com.sparkthis.app.SparkThisApp
import com.sparkthis.app.model.Post
import com.sparkthis.app.ui.addpost.AddPostActivity
import com.sparkthis.app.ui.login.LoginActivity
import com.sparkthis.app.ui.post.PostActivity
import retrofit2.Retrofit
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainActivityContract.View, MainActivityContract.Navigator {

    @BindView(R.id.toolbar) lateinit var toolbar: Toolbar
    @BindView(R.id.fab) lateinit var fab: FloatingActionButton
    @BindView(R.id.posts_recycler_view) lateinit var postsRecyclerView: RecyclerView

    @OnClick(R.id.fab)
    fun onFabClick(view: View) {
        navigateToAddPost()
    }

    @Inject lateinit var presenter: MainActivityPresenter
    @Inject lateinit var retrofit: Retrofit
    @Inject lateinit var postsAdapter: PostsRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ButterKnife.bind(this)

        setSupportActionBar(toolbar)

        (application as SparkThisApp).getUserComponent()?.plus(MainActivityModule(this))?.inject(this)

        postsAdapter.navigator = this

        // setup posts recycler view
        postsRecyclerView.layoutManager = LinearLayoutManager(this)
        postsRecyclerView.setHasFixedSize(true)
        postsRecyclerView.adapter = postsAdapter

    }

    override fun onResume() {
        super.onResume()
        presenter.bindView(this)
        presenter.bindNavigator(this)
        presenter.fetchPosts()
    }

    override fun onStop() {
        super.onStop()
        presenter.unbindView()
        presenter.unbindNavigator()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_login, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.getItemId()
        if (id == R.id.action_logout) {
            presenter.logout()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun releaseUserComponent() {
        (application as SparkThisApp).releaseUserComponent()
    }

    override fun displayPosts(posts: List<Post>) {
        postsAdapter.updatePosts(posts)
    }

    override fun navigateToLogin() {
        startActivity(Intent(MainActivity@this, LoginActivity::class.java))
        finish()
    }

    override fun navigateToAddPost() {
        startActivity(Intent(MainActivity@this, AddPostActivity::class.java))
    }

    override fun navigateToPost(post: Post) {
        val intent = Intent(MainActivity@this, PostActivity::class.java)
        intent.putExtra("id", post.id)
        intent.putExtra("user_id", post.userId)
        intent.putExtra("category_id", post.categoryId)
        intent.putExtra("tell_more_id", post.tellMoreId)
        intent.putExtra("all_coments", post.allComments)
        intent.putExtra("all_votes", post.allVotes)
        intent.putExtra("title", post.title)
        intent.putExtra("short_desc", post.shortDesc)
        intent.putExtra("body", post.body)
        intent.putExtra("created_at", post.createdAt)
        intent.putExtra("updated_at", post.updatedAt)
        startActivity(intent)
    }
}
