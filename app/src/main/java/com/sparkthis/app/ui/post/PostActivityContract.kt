package com.sparkthis.app.ui.post

import com.sparkthis.app.ui.base.BaseNavigator
import com.sparkthis.app.ui.base.BasePresenter
import com.sparkthis.app.ui.base.BaseView

/**
 * Created by Admin on 11/4/2016.
 */
interface PostActivityContract {

    interface View: BaseView {

    }

    interface Navigator: BaseNavigator {

    }

    abstract class Presenter: BasePresenter<View, Navigator>() {

    }
}