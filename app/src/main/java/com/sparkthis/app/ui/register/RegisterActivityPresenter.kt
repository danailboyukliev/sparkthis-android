package com.sparkthis.app.ui.register

import com.sparkthis.app.domain.LoginInteractor
import com.sparkthis.app.domain.RegisterInteractor
import com.sparkthis.app.model.User

/**
 * Created by Admin on 10/25/2016.
 */
class RegisterActivityPresenter(registerInteractor: RegisterInteractor, loginInteractor: LoginInteractor) : RegisterActivityContract.Presenter() {

    private var registerInteractor: RegisterInteractor
    private var loginInteractor: LoginInteractor

    init {
        this.registerInteractor = registerInteractor
        this.loginInteractor = loginInteractor
    }

    fun register(email: String, password: String) {
        registerInteractor.email = email
        registerInteractor.password = password
        registerInteractor.listener = object: RegisterInteractor.Listener {
            override fun onRegisterSuccess(message: String?) {
                if (message != null)
                    view?.showProgressDialog(message, length = 2000)
                login(email, password)
            }

            override fun onRegisterFailure(message: String?) {
                if (message != null) {
                    view?.showProgressDialog(message, length = 2000)
                } else {
                    view?.hideProgressDialog()
                }
            }

            override fun onFailure() {
                view?.hideProgressDialog()
            }
        }
        registerInteractor.execute()
    }

    private fun login(email: String, password: String) {
        loginInteractor.email = email
        loginInteractor.password = password
        loginInteractor.listener = object: LoginInteractor.Listener {
            override fun onLoginFailure(message: String?) {
                if (message != null) {
                    view?.showProgressDialog(message, length = 2000)
                } else {
                    view?.hideProgressDialog()
                }
            }

            override fun onLoginSuccess(message: String?, user: User?) {
                if (message != null)
                    view?.showProgressDialog(message, length = 2000)
                view?.buildUserComponent(user!!)
                navigator?.navigateToMain(delay = 3000)
            }

            override fun onFailure() {
                view?.hideProgressDialog()
            }

        }
        loginInteractor.execute()
    }

}