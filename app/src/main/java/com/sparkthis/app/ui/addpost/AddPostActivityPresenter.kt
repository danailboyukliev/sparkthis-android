package com.sparkthis.app.ui.addpost

import com.sparkthis.app.domain.AddPostInteractor
import com.sparkthis.app.domain.FetchIdeaOptionsInteractor
import com.sparkthis.app.model.IdeaCategory
import com.sparkthis.app.model.TellMoreOption

/**
 * Created by Admin on 10/25/2016.
 */
class AddPostActivityPresenter(fetchTellMoreOptionsInteractor: FetchIdeaOptionsInteractor,
                               addPostInteractor: AddPostInteractor) : AddPostActivityContract.Presenter() {

    private val fetchTellMoreOptionsInteractor: FetchIdeaOptionsInteractor
    private val addPostInteractor: AddPostInteractor

    init {
        this.fetchTellMoreOptionsInteractor = fetchTellMoreOptionsInteractor
        this.addPostInteractor = addPostInteractor
    }

    override fun fetchIdeaOptions() {
        fetchTellMoreOptionsInteractor.listener = object: FetchIdeaOptionsInteractor.Listener {
            override fun onFetchIdeaOptionsSuccess(tellMoreOptions: List<TellMoreOption>, categories: List<IdeaCategory>) {
                view?.displayIdeaOptions(tellMoreOptions, categories)
            }

            override fun onFetchIdeaOptionsFailure() {

            }

            override fun onFailure() {

            }
        }
        fetchTellMoreOptionsInteractor.execute()
    }

    override fun addIdea(title: String, shortDescription: String, body: String, tellMoreOption: TellMoreOption, category: IdeaCategory) {
        addPostInteractor.ideaTitle = title
        addPostInteractor.ideaShortDescription = shortDescription
        addPostInteractor.ideaBody = body
        addPostInteractor.ideaTellMoreOption = tellMoreOption
        addPostInteractor.ideaCategory = category
        addPostInteractor.listener = object: AddPostInteractor.Listener {
            override fun onAddPostSuccess(message: String?) {
                navigator?.navigateBack(delay = 2000)
            }

            override fun onAddPostFailure(message: String?) {
                message?.let {
                    view?.displayError(it)
                    return
                }
            }

            override fun onFailure() {
                view?.displayError("Please try again")
            }
        }
        addPostInteractor.execute()
    }
}