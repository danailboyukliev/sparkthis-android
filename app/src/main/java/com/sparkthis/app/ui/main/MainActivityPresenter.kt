package com.sparkthis.app.ui.main

import com.sparkthis.app.domain.FetchPostsInteractor
import com.sparkthis.app.domain.LogoutInteractor
import com.sparkthis.app.model.Post

/**
 * Created by Admin on 10/21/2016.
 */
class MainActivityPresenter(logoutInteractor: LogoutInteractor, fetchPostsInteractor: FetchPostsInteractor): MainActivityContract.Presenter() {

    private var logoutInteractor: LogoutInteractor
    private var fetchPostsInteractor: FetchPostsInteractor

    init {
        this.logoutInteractor = logoutInteractor
        this.fetchPostsInteractor = fetchPostsInteractor
    }

    override fun fetchPosts() {
        fetchPostsInteractor.listener = object: FetchPostsInteractor.Listener {
            override fun onFetchPostsFailure(message: String?) {

            }

            override fun onFetchPostsSuccess(posts: List<Post>?) {
                posts?.let { posts ->
                    view?.displayPosts(posts)
                }
            }

            override fun onFailure() {

            }
        }
        fetchPostsInteractor.execute()
    }

    override fun logout() {
        logoutInteractor.listener = object: LogoutInteractor.Listener {
            override fun onLogoutSuccess() {
                view?.releaseUserComponent()
                navigator?.navigateToLogin()
            }

            override fun onLogoutFailure() {
                view?.releaseUserComponent()
                navigator?.navigateToLogin()
            }

            override fun onFailure() {
                view?.releaseUserComponent()
                navigator?.navigateToLogin()
            }

        }
        logoutInteractor.execute()
    }
}