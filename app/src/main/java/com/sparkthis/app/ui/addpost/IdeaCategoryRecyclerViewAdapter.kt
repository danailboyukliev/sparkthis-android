package com.sparkthis.app.ui.addpost

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.sparkthis.app.R
import com.sparkthis.app.model.IdeaCategory

/**
 * Created by Admin on 10/26/2016.
 */
class IdeaCategoryRecyclerViewAdapter: RecyclerView.Adapter<IdeaCategoryRecyclerViewAdapter.ViewHolder>() {

    var checked: IdeaCategory? = null

    private val ideaCategories: MutableList<IdeaCategory>

    private var checkboxes: MutableList<CheckBox>

    init {
        this.ideaCategories = mutableListOf()
        this.checkboxes = mutableListOf()
    }

    fun update(ideaCategory: List<IdeaCategory>) {
        this.ideaCategories.addAll(ideaCategory)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: IdeaCategoryRecyclerViewAdapter.ViewHolder?, position: Int) {
        holder?.let { holder ->
            holder.labelTextView?.text = ideaCategories[position].category
            holder.checkbox?.let { checkboxes.add(it) }
            holder.checkbox?.setOnCheckedChangeListener { compoundButton, isChecked: Boolean ->

                // uncheck all other checkboxes
                checkboxes.forEach { it.isChecked = false }

                holder.checkbox?.isChecked = isChecked
                if (isChecked) {
                    checked = ideaCategories[position]
                } else {
                    checked = null
                }
            }
        }
    }

    override fun getItemCount(): Int = ideaCategories.count()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): IdeaCategoryRecyclerViewAdapter.ViewHolder {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_checkbox_grid, parent, false)
        return IdeaCategoryRecyclerViewAdapter.ViewHolder(view)
    }


    class ViewHolder(itemView: View?): RecyclerView.ViewHolder(itemView) {

        var checkbox: CheckBox? = null
        var labelTextView: TextView? = null

        init {
            itemView?.let { itemView ->
                checkbox = itemView.findViewById(R.id.checkbox_value) as CheckBox
                labelTextView = itemView.findViewById(R.id.checkbox_label) as TextView
            }
        }
    }
}