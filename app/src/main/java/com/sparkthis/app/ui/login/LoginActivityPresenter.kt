package com.sparkthis.app.ui.login

import com.sparkthis.app.domain.LoginInteractor
import com.sparkthis.app.model.User

/**
 * Created by Admin on 10/20/2016.
 */
class LoginActivityPresenter(loginInteractor: LoginInteractor) : LoginActivityContract.Presenter() {

    private val loginInteractor: LoginInteractor

    init {
        this.loginInteractor = loginInteractor
    }

    override fun attemptLogin(email: String, password: String) {
        loginInteractor.email = email
        loginInteractor.password = password
        loginInteractor.listener = object: LoginInteractor.Listener {
            override fun onLoginFailure(message: String?) {
                if (message != null) {
                    view?.showProgressDialog(message, length = 2000)
                } else {
                    view?.hideProgressDialog()
                }
            }

            override fun onLoginSuccess(message: String?, user: User?) {
                if (message != null)
                    view?.showProgressDialog(message, length = 2000)
                view?.buildUserComponent(user!!)
                navigator?.navigateToMain(delay = 3000)
            }

            override fun onFailure() {
                view?.hideProgressDialog()
            }

        }
        loginInteractor.execute()
    }

}